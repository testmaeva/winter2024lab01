import java.util.Scanner;
/*2333726*/

public class hangman {
    public static void main(String[] args) {
        Hangman();
    }

    public static void Hangman() {
        Scanner reader = new Scanner(System.in);

        String[] words = {"gtfo", "stfu", "bard", "lame", "swag"};

        System.out.println("Pick a number between 1 and 5");
        int random = reader.nextInt();
        String word = words[random-1];
        System.out.println("Great, let's play!");

        runGame(word);
    }

    public static int isLetterInWord(String word, char c) {

        for (int i=0; i<word.length(); i++) {
            if (toUpperCase(c)==toUpperCase(word.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
        String answer = "0123";

        if (letter0) {
            answer = answer.replace('0', word.charAt(0));
        } else {
            answer = answer.replace('0', '_');
        }
        if (letter1) {
            answer = answer.replace('1', word.charAt(1));
        } else {
            answer = answer.replace('1', '_');
        }
        if (letter2) {
            answer = answer.replace('2', word.charAt(2));
        } else {
            answer = answer.replace('2', '_');
        }
        if (letter3) {
            answer = answer.replace('3', word.charAt(3));
        } else {
            answer = answer.replace('3', '_');
        }
        System.out.println("Your result is: " + answer);
    }

    public static void runGame(String word) {
        Scanner reader = new Scanner(System.in);
        boolean letter0 = false;
        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;
        int hp = 6;

        while (hp > 0 && (!letter0 || !letter1 || !letter2 || !letter3)) {
            System.out.println("Enter a letter:");
            char c = reader.next().charAt(0);
            int index = isLetterInWord(word, c);

            if (index==-1){
                hp--;
            }
            if (index==0) {
                letter0 = true;
            }
            if (index==1){
                letter1 = true;
            }
            if (index==2){
                letter2 = true;
            }
            if (index==3){
                letter3 = true;
            }
            printWork(word, letter0, letter1, letter2, letter3);
            System.out.println("You have " + hp + " lives left, gl!");
        }
        if (hp > 0){
            System.out.println("u won!! good job :]");
        }
        if (hp == 0){
            System.out.println("u lost, sory :[");
        }
    }
}